#!/usr/bin/bash

declare branchName
declare commitid
declare rebuild
declare redeploy
filename=branchControlScript.xml

#format check
#if branchName not start with feature/ and bugfix/, report error
sed -n -e 's/<property //' -e 's/\/>//p' $filename > tmp.txt
cat tmp.txt | while read line
do
    #echo $line
    eval "$line"
    #echo "$branchName--$commitid--$rebuild--$redeploy"
    echo $branchName | egrep -E "^feature/|^bugfix/" 
    if [ $? -ne 0 ];
    then
        echo "branchName $branchName must start with feature/ or bugfix/ string"
        break
        exit 1
    fi
done

#format check
#if commitid is empty, report error
sed -n -e 's/<property //' -e 's/\/>//p' $filename > tmp.txt
cat tmp.txt | while read line
do
    #echo $line
    eval "$line"
    #echo "$branchName--$commitid--$rebuild--$redeploy"

    if [ -z $commitid ];
    then
        echo "commitid must not be empty"
        exit 1
    fi
done

#format check
#if rebuild and redeploy are empty or not yes or no, report error
sed -n -e 's/<property //' -e 's/\/>//p' $filename > tmp.txt
cat tmp.txt | while read line
do
    #echo $line
    eval "$line"
    #echo "$branchName--$commitid--$rebuild--$redeploy"

    if [ -z $rebuild ];
    then
        echo "rebuild must not be empty"
        exit 1
    fi
    if [ -z $redeploy ];
    then
        echo "redeploy must not be empty"
        exit 1
    fi

    echo $rebuild | egrep -i "^yes$|^no$" 
    if [ $? -ne 0 ];
    then
        echo "rebuild $rebuild must be yes or no"
        exit 1
    fi
    echo $redeploy | egrep -i "^yes$|^no$" 
    if [ $? -ne 0 ];
    then
        echo "redeploy $redeploy must be yes or no"
        exit 1
    fi    
done


#processing xml, save all attribute value to $tmp, then save each attribute to it's variable
tmp=`sed -n -e 's/<property //' -e 's/\/>//p' $filename | sed -e 's/branchName="//' -e 's/commitid="//' -e 's/rebuild="//' -e 's/redeploy="//' -e 's/"//g'`
lrc=($(echo $tmp))
for ((i=0,j=0;i<${#lrc[*]};i++,j++))
do
        branchName[j]=${lrc[i]}
        commitid[j]=${lrc[++i]}
        rebuild[j]=${lrc[++i]}
        redeploy[j]=${lrc[++i]}
done

for((k=0; k<${#branchName[*]}; k++))
do         
   echo "branchName: ${branchName[k]}--commitid: ${commitid[k]}--rebuild: ${rebuild[k]}--redeploy：${redeploy[k]}"
done
